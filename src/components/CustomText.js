import React from 'react';
import {Text as NativeText, StyleSheet} from 'react-native';
import colors from '../styles/colors';
import {Fonts, FontSize} from '../styles/fonts';

const styles = StyleSheet.create({
  text: {
    color: colors.textPrimary,
    fontSize: FontSize.body,
    fontFamily: Fonts.robotoRegular,
  },
  colorTextSecondary: {
    color: colors.grayReal,
  },
  colorTextWhite: {
    color: colors.textWhite,
  },
  colorPrimary: {
    color: colors.primary,
  },
  fontSizeSubheading: {
    fontSize: FontSize.subheading,
  },
});

const CustomText = ({color, fontSize, style, ...props}) => {
  const textStyle = [
    styles.text,
    color === 'textSecondary' && styles.colorTextSecondary,
    color === 'primary' && styles.colorPrimary,
    color === 'textWhite' && styles.colorTextWhite,
    fontSize === 'subheading' && styles.fontSizeSubheading,
    style,
  ];

  return <NativeText style={textStyle} {...props} />;
};

export default CustomText;
