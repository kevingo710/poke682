import PropTypes from 'prop-types';
import React from 'react';
import {ImageBackground, StyleSheet, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import colors from '../styles/colors';

const imageUri = require('../assets/images/background/pokebackground.png');

const TemplateScreen = ({isFullScreen, children, backdropOpacity}) => {
  const {top} = useSafeAreaInsets();
  const marginTop = isFullScreen ? 0 : top;
  const source = isFullScreen ? {} : imageUri;

  return (
    <ImageBackground
      source={source}
      style={[
        styles.image,
        backdropOpacity && {backgroundColor: 'rgba(0,0,0,0.5)'},
      ]}>
      <View
        style={{
          ...styles.root,
          marginTop,
        }}>
        {children}
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  image: {
    flex: 1,
    resizeMode: 'cover',
  },
  root: {
    backgroundColor: colors.transparent,
    flex: 1,
  },
});

TemplateScreen.propTypes = {
  children: PropTypes.node.isRequired,
  isFullScreen: PropTypes.bool,
  backdropOpacity: PropTypes.bool,
};

TemplateScreen.defaultProps = {
  isFullScreen: false,
  backdropOpacity: false,
};

export default TemplateScreen;
