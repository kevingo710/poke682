import uuid from 'react-native-uuid';
import {getIdFromUrl} from './helpers';
import {capitalizeWord} from './words';
const mapperRegions = arrayList => {
  return arrayList.map(item => {
    return {
      ...item,
      customId: uuid.v4().toString(),
      id: getIdFromUrl(item.url),
    };
  });
};

const mapperItemsPicker = arrayList => {
  return arrayList.map(item => {
    return {
      value: getIdFromUrl(item.url),
      label: capitalizeWord(item.name),
    };
  });
};

export {mapperRegions, mapperItemsPicker};
