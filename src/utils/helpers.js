const getIdFromUrl = url => {
  const urlParts = url.split('/');
  return Number(urlParts[urlParts.length - 2]);
};

export {getIdFromUrl};
