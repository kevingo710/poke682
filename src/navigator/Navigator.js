import {Platform} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React, {useContext} from 'react';
import HomeScreen from '../screens/HomeScreen';
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from '../styles/colors';
import RegionScreen from '../screens/RegionScreen';
import TeamScreen from '../screens/TeamScreen';
import ProfileScreen from '../screens/ProfileScreen';
import {AuthContext} from '../context/AuthContext';

const Navigator = () => {
  const Tab = createBottomTabNavigator();

  const {user} = useContext(AuthContext);
  let isAuth = user?.idToken || false;

  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          paddingVertical: Platform.OS === 'ios' ? 20 : 0,
          height: 90,
          paddingHorizontal: 5,
          paddingTop: 0,
          backgroundColor: colors.darkBlue,
          position: 'absolute',
          borderTopWidth: 0,
        },
      }}>
      {isAuth ? (
        <>
          <Tab.Screen
            name="Region"
            component={RegionScreen}
            options={{
              tabBarLabel: 'Start',
              tabBarIcon: ({color, size}) => (
                <Icon name="navigation" color={color} size={size} />
              ),
              tabBarLabelPosition: 'beside-icon',
            }}
          />
          <Tab.Screen
            name="Team"
            component={TeamScreen}
            options={{
              tabBarLabel: 'Teams',
              tabBarIcon: ({color, size}) => (
                <Icon name="people" color={color} size={size} />
              ),
              tabBarLabelPosition: 'beside-icon',
            }}
          />
          <Tab.Screen
            name="Profile"
            component={ProfileScreen}
            options={{
              tabBarLabel: 'Profile',
              tabBarIcon: ({color, size}) => (
                <Icon name="person" color={color} size={size} />
              ),
              tabBarLabelPosition: 'beside-icon',
            }}
          />
        </>
      ) : (
        <Tab.Screen
          name="Home"
          component={HomeScreen}
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({color, size}) => (
              <Icon name="home" color={color} size={size} />
            ),
            tabBarLabelPosition: 'beside-icon',
          }}
        />
      )}
    </Tab.Navigator>
  );
};

export default Navigator;
