const Fonts = {
  robotoLight: 'Roboto-Light',
  robotoBold: 'Roboto-Bold',
  robotoItalic: 'Roboto-Italic',
  robotoRegular: 'Roboto-Regular',
};

const FontSize = {
  body: 14,
  subheading: 16,
};

export {Fonts, FontSize};
