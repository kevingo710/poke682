const colors = {
  darkBlue: '#140E36',
  orange: '#FF9D2C',
  gray: '#000000',
  grayReal: '#C7BFBD',
  brown: '#591D0F',
  textWhite: 'white',
  primary: '#0366d6',
  red: 'red',
  transparent: 'transparent',
};

export default colors;
