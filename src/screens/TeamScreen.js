import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import database from '@react-native-firebase/database';
import {useIsFocused} from '@react-navigation/native';
import uuid from 'react-native-uuid';
import colors from '../styles/colors';
import {pokev2Api} from '../api/pokev2Api';
import TemplateScreen from '../components/TemplateScreen';

let itemsRef = database().ref('/teams');

const TeamScreen = () => {
  const isFocused = useIsFocused();
  const [itemsArray, setItemsArray] = React.useState([]);
  const [listNames, setListNames] = useState([]);

  const loadPokemonName = async listPokemon => {
    let promiseArray = listPokemon.map(element =>
      pokev2Api.get(`https://pokeapi.co/api/v2/pokemon/${element}`),
    );
    try {
      const gistsDescriptions = (await Promise.all(promiseArray)).map(
        res => res.data.name,
      );
      setListNames(gistsDescriptions);
      return gistsDescriptions;
    } catch (error) {
      console.error(error);
    }
  };

  const renderItem = ({item}) => {
    return (
      <View style={styles.item}>
        <Text style={styles.title}>{`${item?.teamName}`}</Text>
        <Text style={styles.subtitle}>{item?.teamNumber}</Text>
        <Text style={styles.subtitle}>{item?.teamType}</Text>
        <Text style={styles.subtitle}>{item?.pokedexDescription}</Text>
        <View style={{flexDirection: 'row'}}>
          {item.listPokemon.map(element => {
            return (
              <>
                <Image
                  key={uuid.v4().toString() + element}
                  style={{width: 50, height: 50, marginRight: 5}}
                  source={{
                    uri: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${element}.png`,
                  }}
                />
              </>
            );
          })}
        </View>
        {/* <View style={{flexDirection: 'row'}}>
          {listNames &&
            listNames.map(element => {
              return <Text style={styles.subtitle}>{element}</Text>;
            })}
        </View> */}
      </View>
    );
  };

  useEffect(() => {
    if (isFocused) {
      itemsRef.on('value', snapshot => {
        let data = snapshot.val() || {};
        const items = Object?.values(data) || [];
        console.log({items});
        setItemsArray(items);
      });
    }
  }, [isFocused]);

  return (
    <TemplateScreen>
      <Text>TeamScreen</Text>
      {itemsArray && (
        <FlatList
          data={itemsArray}
          renderItem={renderItem}
          keyExtractor={item => item.teamNumber}
        />
      )}
    </TemplateScreen>
  );
};

export default TeamScreen;

const styles = StyleSheet.create({
  item: {
    backgroundColor: colors.grayReal,
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 15,
  },
  title: {
    fontSize: 32,
  },
  subtitle: {
    fontSize: 12,
    marginRight: 8,
  },
});
