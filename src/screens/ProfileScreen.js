import {StyleSheet, Text, View, Image} from 'react-native';
import React, {useContext} from 'react';
import TemplateScreen from '../components/TemplateScreen';
import {AuthContext} from '../context/AuthContext';
import CustomText from '../components/CustomText';
import colors from '../styles/colors';

const ProfileScreen = () => {
  const {user} = useContext(AuthContext);

  return (
    <TemplateScreen>
      <Text>ProfileScreen</Text>
      <View style={styles.profileContainer}>
        <Image
          style={styles.imageProfile}
          source={{
            uri: user.user.photo,
          }}
        />
        <CustomText>{user.user.name}</CustomText>
        <CustomText>{user.user.email}</CustomText>
      </View>
    </TemplateScreen>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  profileContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.textWhite,
    width: 200,
    height: 200,
    borderRadius: 100,
    alignSelf: 'center',
  },
  imageProfile: {width: 50, height: 50, marginRight: 5, borderRadius: 25},
});
