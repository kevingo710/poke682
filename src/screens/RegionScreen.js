import {
  Button,
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import uuid from 'react-native-uuid';
import DropDownPicker from 'react-native-dropdown-picker';
import RNPickerSelect from 'react-native-picker-select';
import database from '@react-native-firebase/database';
import crashlytics from '@react-native-firebase/crashlytics';

import {pokev2Api} from '../api/pokev2Api';
import {mapperItemsPicker, mapperRegions} from '../utils/mappers';
import colors from '../styles/colors';
import {Formik} from 'formik';
import TemplateScreen from '../components/TemplateScreen';
import {useNavigation} from '@react-navigation/native';

const initialValues = {
  teamName: '',
  teamNumber: uuid.v4().toString(),
  teamType: null,
  pokedexDescription: '',
  teamRegion: '',
};

const RegionScreen = () => {
  const [totalRegions, setTotalRegions] = useState(0);
  const [regions, setRegions] = useState([]);
  const navigation = useNavigation();
  const [open, setOpen] = useState(false);
  const [openRegion, setOpenRegion] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([
    {label: 'Apple', value: 'apple'},
    {label: 'Banana', value: 'banana'},
  ]);
  const [itemsRegions, setItemsRegions] = useState([]);
  const [pickerListPokemons, setPickerListPokemons] = useState([]);
  const [valueRegion, setValueRgion] = useState(null);
  const loadRegions = async () => {
    const response = await pokev2Api.get('https://pokeapi.co/api/v2/region/');
    console.log(mapperRegions(response.data.results));
    setRegions(response.data.results);
    setItemsRegions(mapperItemsPicker(response.data.results));
    setTotalRegions(response.data.count);
  };

  const loadPokemosByRegion = async id => {
    const response = await pokev2Api.get(
      `https://pokeapi.co/api/v2/region/${id}`,
    );
    const listPokedexesUrl = response.data.pokedexes.map(item => item.url);
    let promiseArray = listPokedexesUrl.map(url => pokev2Api.get(url));
    try {
      const gistsDescriptions = (await Promise.all(promiseArray)).map(
        res => res.data.pokemon_entries,
      );
      const flatResult = gistsDescriptions.flat();

      const listSpecies = flatResult.map(item => item.pokemon_species);
      const filterListPokemons = listSpecies.filter(
        (v, i, a) =>
          a.findIndex(v2 => ['name'].every(k => v2[k] === v[k])) === i,
      );
      setPickerListPokemons(mapperItemsPicker(filterListPokemons));
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    loadRegions();
  }, []);

  const onSubmit = async (
    values,
    {setSubmitting, setErrors, setStatus, resetForm},
  ) => {
    console.log(values);
    let newValues = {
      ...values,
      listPokemon: [
        values.pokemonOne,
        values.pokemonTwo,
        values.pokemonThree,
        values.pokemonFour,
        values.pokemonFive,
        values.pokemonSix,
      ],
    };
    console.log(newValues);
    const newReference = database().ref('/teams').push();
    try {
      await newReference.set(newValues);
      resetForm({});
      setStatus({success: true});
      navigation.navigate('Team');
    } catch (error) {
      setStatus({success: false});
      setSubmitting(false);
      setErrors({submit: error.message});
    }
  };

  return (
    <TemplateScreen>
      <View style={{flex: 1, marginTop: 50}}>
        <Text style={{color: colors.orange}}>CREATE TEAM</Text>
      </View>
      <View style={{flex: 8, marginLeft: 20}}>
        <Formik initialValues={initialValues} onSubmit={onSubmit}>
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            values,
            setFieldValue,
          }) => (
            <View>
              <TextInput
                style={styles.textInput}
                placeholder="Team Name"
                onChangeText={handleChange('teamName')}
                onBlur={handleBlur('teamName')}
                value={values.teamName}
              />
              <TextInput
                style={[styles.textInput, {color: colors.grayReal}]}
                editable={false}
                onChangeText={handleChange('teamNumber')}
                onBlur={handleBlur('teamNumber')}
                value={values.teamNumber}
              />
              <DropDownPicker
                placeholder="Select a Team Type"
                open={open}
                value={value}
                items={items}
                setOpen={setOpen}
                setValue={value => {
                  setFieldValue('teamType', value());
                  setValue(value);
                }}
                setItems={setItems}
                style={{width: 300}}
                dropDownContainerStyle={{width: 300}}
              />
              <TextInput
                style={styles.textInput}
                onChangeText={handleChange('pokedexDescription')}
                placeholder="Pokedex Description"
                onBlur={handleBlur('pokedexDescription')}
                value={values.pokedexDescription}
              />
              {itemsRegions && (
                <DropDownPicker
                  zIndex={10}
                  placeholder="Select a Region"
                  open={openRegion}
                  value={valueRegion}
                  items={itemsRegions}
                  setOpen={setOpenRegion}
                  setValue={value => {
                    console.log(value());
                    setFieldValue('teamRegion', value());
                    setValueRgion(value);
                    loadPokemosByRegion(value());
                  }}
                  style={{width: 300}}
                  dropDownContainerStyle={{width: 300}}
                />
              )}

              {pickerListPokemons && (
                <>
                  <View style={{flexDirection: 'row'}}>
                    <View
                      style={{
                        width: 120,
                        height: 45,

                        margin: 12,
                      }}>
                      <RNPickerSelect
                        placeholder={{
                          label: 'Pokemon 1',
                          value: null,
                        }}
                        style={{
                          ...pickerSelectStyles,
                          iconContainer: {
                            top: 20,
                            right: 10,
                          },
                          placeholder: {
                            color: 'white',
                            fontSize: 12,
                            fontWeight: 'bold',
                          },
                        }}
                        onValueChange={value => {
                          console.log(value);

                          setFieldValue('pokemonOne', value);
                        }}
                        items={pickerListPokemons}
                        Icon={() => {
                          return (
                            <View
                              style={{
                                backgroundColor: 'transparent',
                                borderTopWidth: 10,
                                borderTopColor: 'gray',
                                borderRightWidth: 10,
                                borderRightColor: 'transparent',
                                borderLeftWidth: 10,
                                borderLeftColor: 'transparent',
                                width: 0,
                                height: 0,
                              }}
                            />
                          );
                        }}
                      />
                    </View>
                    <View
                      style={{
                        width: 120,
                        height: 45,

                        margin: 12,
                      }}>
                      <RNPickerSelect
                        placeholder={{
                          label: 'Pokemon 2',
                          value: null,
                        }}
                        style={{
                          ...pickerSelectStyles,
                          iconContainer: {
                            top: 20,
                            right: 10,
                          },
                          placeholder: {
                            color: 'white',
                            fontSize: 12,
                            fontWeight: 'bold',
                          },
                        }}
                        onValueChange={value => {
                          console.log(value);
                          setFieldValue('pokemonTwo', value);
                        }}
                        items={pickerListPokemons}
                        Icon={() => {
                          return (
                            <View
                              style={{
                                backgroundColor: 'transparent',
                                borderTopWidth: 10,
                                borderTopColor: 'gray',
                                borderRightWidth: 10,
                                borderRightColor: 'transparent',
                                borderLeftWidth: 10,
                                borderLeftColor: 'transparent',
                                width: 0,
                                height: 0,
                              }}
                            />
                          );
                        }}
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      flexGrow: 1,
                      flexShrink: 1,
                    }}>
                    <View
                      style={{
                        width: 120,
                        height: 45,

                        margin: 12,
                      }}>
                      <RNPickerSelect
                        placeholder={{
                          label: 'Pokemon 3',
                          value: null,
                        }}
                        style={{
                          ...pickerSelectStyles,
                          iconContainer: {
                            top: 20,
                            right: 10,
                          },
                          placeholder: {
                            color: 'white',
                            fontSize: 12,
                            fontWeight: 'bold',
                          },
                        }}
                        onValueChange={value => {
                          console.log(value);
                          setFieldValue('pokemonThree', value);
                        }}
                        items={pickerListPokemons}
                        Icon={() => {
                          return (
                            <View
                              style={{
                                backgroundColor: 'transparent',
                                borderTopWidth: 10,
                                borderTopColor: 'gray',
                                borderRightWidth: 10,
                                borderRightColor: 'transparent',
                                borderLeftWidth: 10,
                                borderLeftColor: 'transparent',
                                width: 0,
                                height: 0,
                              }}
                            />
                          );
                        }}
                      />
                    </View>
                    <View
                      style={{
                        width: 120,
                        height: 45,

                        margin: 12,
                      }}>
                      <RNPickerSelect
                        placeholder={{
                          label: 'Pokemon 4',
                          value: null,
                        }}
                        style={{
                          ...pickerSelectStyles,
                          iconContainer: {
                            top: 20,
                            right: 10,
                          },
                          placeholder: {
                            color: 'white',
                            fontSize: 12,
                            fontWeight: 'bold',
                          },
                        }}
                        onValueChange={value => {
                          console.log(value);
                          setFieldValue('pokemonFour', value);
                        }}
                        items={pickerListPokemons}
                        Icon={() => {
                          return (
                            <View
                              style={{
                                backgroundColor: 'transparent',
                                borderTopWidth: 10,
                                borderTopColor: 'gray',
                                borderRightWidth: 10,
                                borderRightColor: 'transparent',
                                borderLeftWidth: 10,
                                borderLeftColor: 'transparent',
                                width: 0,
                                height: 0,
                              }}
                            />
                          );
                        }}
                      />
                    </View>
                  </View>
                  <View>
                    <View
                      style={{
                        width: 120,
                        height: 45,

                        margin: 12,
                      }}>
                      <RNPickerSelect
                        placeholder={{
                          label: 'Pokemon 5',
                          value: null,
                        }}
                        style={{
                          ...pickerSelectStyles,
                          iconContainer: {
                            top: 20,
                            right: 10,
                          },
                          placeholder: {
                            color: 'white',
                            fontSize: 12,
                            fontWeight: 'bold',
                          },
                        }}
                        onValueChange={value => {
                          console.log(value);
                          setFieldValue('pokemonFive', value);
                        }}
                        items={pickerListPokemons}
                        Icon={() => {
                          return (
                            <View
                              style={{
                                backgroundColor: 'transparent',
                                borderTopWidth: 10,
                                borderTopColor: 'gray',
                                borderRightWidth: 10,
                                borderRightColor: 'transparent',
                                borderLeftWidth: 10,
                                borderLeftColor: 'transparent',
                                width: 0,
                                height: 0,
                              }}
                            />
                          );
                        }}
                      />
                    </View>
                    <View
                      style={{
                        width: 120,
                        height: 45,

                        margin: 12,
                      }}>
                      <RNPickerSelect
                        placeholder={{
                          label: 'Pokemon 6',
                          value: null,
                        }}
                        style={{
                          ...pickerSelectStyles,
                          iconContainer: {
                            top: 20,
                            right: 10,
                          },
                          placeholder: {
                            color: 'white',
                            fontSize: 12,
                            fontWeight: 'bold',
                          },
                        }}
                        onValueChange={value => {
                          console.log(value);
                          setFieldValue('pokemonSix', value);
                        }}
                        items={pickerListPokemons}
                        Icon={() => {
                          return (
                            <View
                              style={{
                                backgroundColor: 'transparent',
                                borderTopWidth: 10,
                                borderTopColor: 'gray',
                                borderRightWidth: 10,
                                borderRightColor: 'transparent',
                                borderLeftWidth: 10,
                                borderLeftColor: 'transparent',
                                width: 0,
                                height: 0,
                              }}
                            />
                          );
                        }}
                      />
                    </View>
                  </View>
                </>
              )}
              <Button onPress={handleSubmit} title="Create Team" />
              <Button
                title="Test Crash"
                onPress={() => crashlytics().crash()}
              />
            </View>
          )}
        </Formik>
      </View>
      {/* <View style={{flex: 8}}>
        {regions && (
          <FlatList
            data={regions}
            renderItem={renderItem}
            keyExtractor={item => item.id}
          />
        )}
      </View> */}
    </TemplateScreen>
  );
};

export default RegionScreen;

const styles = StyleSheet.create({
  item: {
    backgroundColor: colors.grayReal,
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  textInput: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: colors.brown,
    width: 300,
    height: 30,
    borderRadius: 7,
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'white',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});
