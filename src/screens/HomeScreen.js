import React, {useContext, useEffect} from 'react';
import type {Node} from 'react';

import {StatusBar, StyleSheet} from 'react-native';

import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
import crashlytics from '@react-native-firebase/crashlytics';

import {AuthContext} from '../context/AuthContext';
import {useNavigation} from '@react-navigation/native';
import CustomText from '../components/CustomText';
import TemplateScreen from '../components/TemplateScreen';

const HomeScreen: () => Node = () => {
  const navigation = useNavigation();
  const {user, setUser} = useContext(AuthContext);

  const handleGoogleSignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      setUser(userInfo);
      navigation.navigate('Region');

      // Create a Google credential with the token
      const googleCredential = auth.GoogleAuthProvider.credential(
        userInfo?.idToken,
      );

      crashlytics().log('User signed in.');
      await Promise.all([
        crashlytics().setUserId(userInfo.user.id),
        crashlytics().setAttribute('credits', String(user.credits)),
        crashlytics().setAttributes({
          email: userInfo.user.email,
          username: userInfo.user.name,
        }),
      ]);

      // Sign-in the user with the credential
      return auth().signInWithCredential(googleCredential);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('cancelled');
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  useEffect(() => {
    GoogleSignin.configure({
      iosClientId:
        '746373279548-6mvakbrctfqf7e29klbrq22rgbhqjnjf.apps.googleusercontent.com',
      webClientId:
        '746373279548-6mvakbrctfqf7e29klbrq22rgbhqjnjf.apps.googleusercontent.com',
      offlineAccess: false,
    });
  }, []);

  return (
    <TemplateScreen>
      <StatusBar barStyle={'light-content'} />

      <GoogleSigninButton
        style={styles.googleButton}
        size={GoogleSigninButton.Size.Wide}
        color={GoogleSigninButton.Color.Dark}
        onPress={handleGoogleSignIn}
      />

      <CustomText>Pokemon App</CustomText>
    </TemplateScreen>
  );
};

const styles = StyleSheet.create({
  googleButton: {
    width: 192,
    height: 48,
    alignSelf: 'center',
    position: 'absolute',
    top: 200,
  },
});

export default HomeScreen;
