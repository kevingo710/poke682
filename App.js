/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import type {Node} from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {SafeAreaView, StatusBar, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import colors from './src/styles/colors';
import Navigator from './src/navigator/Navigator';
import {AuthProvider} from './src/context/AuthProvider';

const MyTheme = {
  dark: false,
  colors: {
    primary: colors.orange,
    background: colors.darkBlue,
    card: 'rgb(255, 255, 255)',
    text: colors.gray,
    border: colors.darkBlue,
    notification: colors.orange,
  },
};

const App: () => Node = () => {
  return (
    <NavigationContainer theme={MyTheme}>
      <AuthProvider>
        <Navigator />
        {/* <SafeAreaView>
        <StatusBar />
        <Icon name="home" color={'red'} size={50} />
        <Text style={{fontFamily: 'Roboto-Light'}}>Mi increíble fuente</Text>
      </SafeAreaView> */}
      </AuthProvider>
    </NavigationContainer>
  );
};

export default App;
